﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Data;
using System.IO;
using System.Text;

public class DataPrint : MonoBehaviour
{
    
    private float indexSignalIcons = 1;
    private bool enableAnimation = false;
    private float animationInterval = 0.06f;

    ThinkGearController controller;

    private int Raw = 0;
    private int PoorSignal = 200;
    private int Attention = 0;
    private int Meditation = 0;
    private int Blink = 0;
    private float Delta = 0.0f;
    private float Theta = 0.0f;
    private float LowAlpha = 0.0f;
    private float HighAlpha = 0.0f;
    private float LowBeta = 0.0f;
    private float HighBeta = 0.0f;
    private float LowGamma = 0.0f;
    private float HighGamma = 0.0f;
    public Text lebel;
    float dtime;
    int preAtt;

    struct ThinkData { public string date, time; public int att, medi, poorsig; }
    List<ThinkData> ThinkDataList;
    bool isExported;
    DataTable workTable;
    
    // Use this for initialization
    void Start()
    {
        workTable = new DataTable("Customers");
        //customers = new DataSet();
        //customersTable = customers.Tables.Add("CustomersTable");
        /*DataColumn workCol = workTable.Columns.Add("CustID", typeof(Int32));
        workCol.AllowDBNull = false;
        workCol.Unique = true;*/

        workTable.Columns.Add("Date", typeof(String));
        workTable.Columns.Add("Time", typeof(String));
        workTable.Columns.Add("Attention", typeof(Int32));
        workTable.Columns.Add("Meditation", typeof(Int32));
        workTable.Columns.Add("PoorSignal", typeof(Int32));

#if UNITY_EDITOR
        lebel.text = "PC";
#elif UNITY_IPHONE
		lebel.text = "IOS";
#elif UNITY_ANDROID
		lebel.text = "ANDROID"+"\n"+Application.persistentDataPath;
        //lebel.text=Application.persistentDataPath;
#endif
        ThinkDataList = new List<ThinkData>();
        isExported = false;

        controller = GameObject.Find("ThinkGear").GetComponent<ThinkGearController>();
        //lebel = GameObject.Find("Lebel").GetComponent<Text>();

        controller.UpdateRawdataEvent += OnUpdateRaw;
        controller.UpdatePoorSignalEvent += OnUpdatePoorSignal;
        controller.UpdateAttentionEvent += OnUpdateAttention;
        controller.UpdateMeditationEvent += OnUpdateMeditation;

        controller.UpdateDeltaEvent += OnUpdateDelta;
        controller.UpdateThetaEvent += OnUpdateTheta;

        controller.UpdateHighAlphaEvent += OnUpdateHighAlpha;
        controller.UpdateHighBetaEvent += OnUpdateHighBeta;
        controller.UpdateHighGammaEvent += OnUpdateHighGamma;

        controller.UpdateLowAlphaEvent += OnUpdateLowAlpha;
        controller.UpdateLowBetaEvent += OnUpdateLowBeta;
        controller.UpdateLowGammaEvent += OnUpdateLowGamma;

        controller.UpdateBlinkEvent += OnUpdateBlink;
        dtime = 0.0f;
        preAtt = 0;
    }

    void OnUpdatePoorSignal(int value)
    {
        PoorSignal = value;
        if (value == 0)
        {
            indexSignalIcons = 0;
            enableAnimation = false;
        }
        else if (value == 200)
        {
            indexSignalIcons = 1;
            enableAnimation = false;
        }
        else if (!enableAnimation)
        {
            indexSignalIcons = 2;
            enableAnimation = true;
        }
    }
    void OnUpdateRaw(int value)
    {
        Raw = value;
    }
    void OnUpdateAttention(int value)
    {
        Attention = value;
    }
    void OnUpdateMeditation(int value)
    {
        Meditation = value;
    }
    void OnUpdateDelta(float value)
    {
        Delta = value;
    }
    void OnUpdateTheta(float value)
    {
        Theta = value;
    }
    void OnUpdateHighAlpha(float value)
    {
        HighAlpha = value;
    }
    void OnUpdateHighBeta(float value)
    {
        HighBeta = value;
    }
    void OnUpdateHighGamma(float value)
    {
        HighGamma = value;
    }
    void OnUpdateLowAlpha(float value)
    {
        LowAlpha = value;
    }
    void OnUpdateLowBeta(float value)
    {
        LowBeta = value;
    }
    void OnUpdateLowGamma(float value)
    {
        LowGamma = value;
    }

    void OnUpdateBlink(int value)
    {
        Blink = value;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Application.persistentDataPath);
        dtime += Time.deltaTime;
        if (dtime > 1.0f && !isExported)
        {
            AddNewData();
            dtime = 0.0f;
        }
        /*if (preAtt != Attention)//數值有變化
        {
            //text.text = "deltaTime:" + dtime;
            dtime = 0.0f;
        }
        preAtt = Attention;*/
    }

    /**
	 *when Fixed Timestep == 0.02 
	 **/
    void FixedUpdate()
    {
        if (enableAnimation)
        {
            if (indexSignalIcons >= 4.8)
            {
                indexSignalIcons = 2;
            }
            indexSignalIcons += animationInterval;
        }

    }
    
    public void WriteCSV()
    {
        string outputDir;
        string month, day, hour, min;
        if (DateTime.Now.Month < 10) month = "0" + DateTime.Now.Month;
        else month = DateTime.Now.Month.ToString();
        if (DateTime.Now.Day < 10) day = "0" + DateTime.Now.Day;
        else day = DateTime.Now.Day.ToString();
        if (DateTime.Now.Hour < 10) hour = "0" + DateTime.Now.Hour;
        else hour = DateTime.Now.Hour.ToString();
        if (DateTime.Now.Minute < 10) min = "0" + DateTime.Now.Minute;
        else min = DateTime.Now.Minute.ToString();
#if UNITY_EDITOR
        outputDir = "C:/Users/Administrator/Desktop";
#elif UNITY_IPHONE
		
#elif UNITY_ANDROID
		outputDir=Application.persistentDataPath;
        //outputDir="/storage/sdcard0/Download";
        //outputDir="/sdcard/Download";
#endif
        string name = DateTime.Now.Year + month + day + "_" + hour + min + ".csv";
        outputDir = outputDir + "/" + name;

        if(SaveCSV(workTable, outputDir))
        {
            isExported = true;
            GameObject.Find("Text").GetComponent<Text>().text = "EXPORTED!!";
        }
        else
        {
            GameObject.Find("Text").GetComponent<Text>().text = "EXPORT ERROR!!";
        }
    }
    void AddNewData()
    {
        ThinkData data;
        string month, day, hour, min, sec;
        if (DateTime.Now.Month < 10) month = "0" + DateTime.Now.Month;
        else month = DateTime.Now.Month.ToString();
        if (DateTime.Now.Day < 10) day = "0" + DateTime.Now.Day;
        else day = DateTime.Now.Day.ToString();
        if (DateTime.Now.Hour < 10) hour = "0" + DateTime.Now.Hour;
        else hour = DateTime.Now.Hour.ToString();
        if (DateTime.Now.Minute < 10) min = "0" + DateTime.Now.Minute;
        else min = DateTime.Now.Minute.ToString();
        if (DateTime.Now.Second < 10) sec = "0" + DateTime.Now.Second;
        else sec = DateTime.Now.Second.ToString();

        data.date = DateTime.Now.Year + "/" + month + "/" + day;
        data.time = hour + ":" + min + ":" + sec;
        data.att = Attention;
        data.medi = Meditation;
        data.poorsig = PoorSignal;
        ThinkDataList.Add(data);
        //Debug.Log(data.time+"   "+ThinkDataList.Count);
        
        DataRow workRow = workTable.NewRow();
        workRow[0] = data.date;
        workRow[1] = data.time;
        workRow[2] = data.att;
        workRow[3] = data.medi;
        workRow[4] = data.poorsig;
        if (data.poorsig == 200)//如果訊號極差
        {
            //do nothing
        }
        else
        {
            workTable.Rows.Add(workRow);
        }
        

    }  
    public static bool SaveCSV(DataTable dt, string fullPath)
    {
        try
        {
            FileInfo fi = new FileInfo(fullPath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }
            FileStream fs = new FileStream(fullPath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            //StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
            string data = "";
            //寫出列名稱
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                data += "\"" + dt.Columns[i].ColumnName.ToString() + "\"";
                if (i < dt.Columns.Count - 1)
                {
                    data += ",";
                }
            }
            sw.WriteLine(data);
            //寫出各行數據

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                data = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    string str = dt.Rows[i][j].ToString();
                    str = string.Format("\"{0}\"", str);
                    data += str;
                    if (j < dt.Columns.Count - 1)
                    {
                        data += ",";
                    }
                }
                sw.WriteLine(data);
            }
            sw.Close();
            fs.Close();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public static DataTable OpenCSV(string filePath)
    {
        DataTable dt = new DataTable();
        FileStream fs = new FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        StreamReader sr = new StreamReader(fs, Encoding.UTF8);
        //StreamReader sr = new StreamReader(fs, encoding);
        //string fileContent = sr.ReadToEnd();
        //記錄每次讀取的一行記錄
        string strLine = "";
        //記錄每行記錄中的各字段內容
        string[] aryLine = null;
        string[] tableHead = null;
        //標示列數
        int columnCount = 0;
        //標示是否是讀取的第一行
        bool IsFirst = true;
        //逐行讀取CSV中的數據
        while ((strLine = sr.ReadLine()) != null)
        {
            if (IsFirst == true)
            {
                tableHead = strLine.Split(',');
                IsFirst = false;
                columnCount = tableHead.Length;
                //創建列
                for (int i = 0; i < columnCount; i++)
                {
                    tableHead[i] = tableHead[i].Replace("\"", "");
                    DataColumn dc = new DataColumn(tableHead[i]);
                    dt.Columns.Add(dc);
                }
            }
            else
            {
                aryLine = strLine.Split(',');
                DataRow dr = dt.NewRow();
                for (int j = 0; j < columnCount; j++)
                {
                    dr[j] = aryLine[j].Replace("\"", "");
                }
                dt.Rows.Add(dr);
            }
        }
        if (aryLine != null && aryLine.Length > 0)
        {
            dt.DefaultView.Sort = tableHead[2] + " " + "DESC";
        }
        sr.Close();
        fs.Close();
        return dt;
    }
}
